module.exports = {
  siteMetadata: {
    title: `Hector Anadon Consul - Hacalox`,
  },
  plugins: [`gatsby-plugin-react-helmet`],
}

module.exports = {
  // previous setup
    siteMetadata: {
      title: `Hector Anadon Consul - Hacalox`,
    },
    plugins: [
    'gatsby-plugin-catch-links',
    'gatsby-plugin-react-helmet',
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        path: `${__dirname}/src/pages`,
        name: 'pages',
      },
    },
    {
      resolve: 'gatsby-transformer-remark',
      options: {
        plugins: [
          {
            resolve: 'gatsby-remark-images',
            options: {
              linkImagesToOriginal: false
            }
          }
        ]
      }
    },
    'gatsby-plugin-react-helmet',
    'gatsby-plugin-sharp'
  ],
}
