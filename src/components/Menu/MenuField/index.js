import React from 'react'
import './MenuField.css';
import Link from 'gatsby-link'
import { navigateTo } from "gatsby-link"

function linking(url){
  setTimeout(function () {
    navigateTo(url)
  }, 800);
}

export const MenuField = (props) => {
  if(props.external){
    return(
      <a className="MenuLink"
      href={props.url}>
        <h3 className='MenuTab'>{props.text}</h3>
      </a>
    )}

    else if(props.landing){
      return(
        <div className="MenuLink"
        onClick={() => {
          props.onChange(props.state)
          linking(props.url)
        }}>
          <h3 className={props.pressed ? 'MenuTabPressed' : 'MenuTab'}>{props.text}</h3>
        </div>
      )
    }

    else{
    return(<Link className="MenuLink"
      to={props.pressed ? '/' : props.url}
    >
      <h3 className={props.pressed ? 'MenuTabPressed' : 'MenuTab'}>{props.text}</h3>
    </Link>
  )}
}
