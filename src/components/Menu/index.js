import React from 'react'
import './Menu.css';
import {MenuField} from './MenuField'
import {Grid, Row, Col} from 'react-flexbox-grid'
import Link from 'gatsby-link'


export default function Menu(props){
  if(props.location){
    var showBio = props.location === '/bio/'
    var showProjects = props.location === '/projects/'
    var showBlog = props.location === '/blog/'
  }
  else{
    var showBio = props.showBio;
    var showProjects = props.showProjects;
    var shwoBlog = props.showBlog;
  }
return(
  <Grid className='MenuGrid'>
    <Row className='Menu'>
      <Col xs={6} sm={3} md={3} lg={3}>
        <MenuField pressed={showBio} url='/bio/' landing={props.landing} onChange={props.onChange} state='showBio' text='Me' />
      </Col>
      <Col xs={6} sm={3} md={3} lg={3}>
        <MenuField pressed={showProjects} url='/projects/' landing={props.landing} onChange={props.onChange} state='showProjects' text='Projects' />
      </Col>
      <Col xs={6} sm={3} md={3} lg={3}>
        <MenuField pressed={showBlog} url='/blog/' landing={props.landing} onChange={props.onChange} state='showBlog' text='Blog' />
      </Col>
      <Col xs={6} sm={3} md={3} lg={3}>
        <MenuField external={true} url='https://gitlab.com/hacalox/hacalox' onChange="" text='Source' />
      </Col>
    </Row>
  {!props.landing && <hr className={props.selected ? "MenuHrAnimated" : "MenuHrNormal"}/>}
  </Grid>
  )
}
