import React from 'react'
import './HeroLanding.css';
import {Grid, Row, Col} from 'react-flexbox-grid'

const HeroLanding = (props) => (
  <Grid className={props.selected ? "HeroLandingOut" : "HeroLanding"}>
    <Row>
      <Col xs={12}>
        <h1 className="HeroLandingTitle">Hey, I'm <br className="HeroLandingBreak"></br> Hacalox :) </h1>
        <h2 className="HeroLandingSubtitle">My name is <br className="HeroLandingBreak2"></br>Héctor Anadón Cónsul <br className="HeroLandingBreak"></br>and I love creating new products!</h2>
        <a className="CurrentlyLink" href="https://eink.news">
          <h2 className="Currently">I'm currently <br className="HeroLandingBreak2"></br> focused on <br className="HeroLandingBreak"></br> eink.news</h2>
        </a>
      </Col>
    </Row>
  </Grid>
)

export default HeroLanding;
