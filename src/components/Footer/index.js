import React from 'react'
import './Footer.css';
import Gitlab from '../../assets/gitlab_60x60.png'
import Github from '../../assets/github_60x60.png'
import Linkedin from '../../assets/linkedin_50x50.png'
const  Footer = (props) => {
  return(

  <div className="Footer">
    <a href="https://linkedin.com/in/hectoranadon">
      <img className="FooterFirstColumn" src={Linkedin} alt="linkedin-logo" height="50" width="50"/>
    </a>
    <a href="https://gitlab.com/hacalox">
      <img className="FooterSecondColumn" src={Gitlab} alt="gitlab-logo" height="60" width="60"/>
    </a>
    <a href="https://github.com/hacalox">
      <img className="FooterThirdColumn" src={Github} alt="github-logo" height="60" width="60"/>
    </a>
  </div>

  )
}

export default Footer;
