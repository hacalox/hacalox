import React from 'react'
import './SocialHeader.css';
import Gitlab from '../../../assets/gitlab_60x60.png'
import Github from '../../../assets/github_60x60.png'
import Linkedin from '../../../assets/linkedin_50x50.png'
const  SocialHeader = (props) => {
  return(
  <div className="SocialHeader">
    <a href="https://linkedin.com/in/hectoranadon">
      <img className="SocialHeaderFirstColumn" src={Linkedin} alt="linkedin-logo" height="50" width="50"/>
    </a>
    <a href="https://gitlab.com/hacalox">
      <img className="SocialHeaderlSecondColumn" src={Gitlab} alt="gitlab-logo" height="60" width="60"/>
    </a>
    <a href="https://github.com/hacalox">
      <img className="SocialHeaderThirdColumn" src={Github} alt="github-logo" height="60" width="60"/>
    </a>
  </div>
  )
}

export default SocialHeader;
