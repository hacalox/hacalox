import React from 'react'
import './Header.css'
import SocialHeader from './SocialHeader/'
const Header = (props) => {
  return (
      <div className={props.selected ? "HeaderDivOut" : "HeaderDiv"} >
        <SocialHeader/>
        <div className="HeaderEmailDiv">
          <a className="Email" href="mailto:hacalox@hacalox.com">
            email: hacalox@hacalox.com
          </a>
        </div>
      </div>
  )
}

export default Header;
