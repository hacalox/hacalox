import React,{Component} from 'react'
import PropTypes from 'prop-types'
import Helmet from 'react-helmet'
import Menu from '../components/Menu'
import Header from '../components/Header'
import './index.css'
var margin = '64px'
// const pathname = this.props.location.pathname

export default class Template extends Component{
render(){
  {margin = this.props.location.pathname === '/' && '0px'}
  return(
  <div>
    <Helmet
      title="Hector Anadon Consul - Hacalox"
      meta={[
        { name: 'description', content: 'Sample' },
        { name: 'keywords', content: 'sample, something' },
      ]}
    />
    {this.props.location.pathname !== '/' && <Menu location={this.props.location.pathname} />}
    <div
      style={{
        margin: {margin}+' auto',
        maxWidth: '100%'
      }}
    >
      {this.props.children()}
    </div>
  </div>
)}
}
