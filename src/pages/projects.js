import React from 'react'
import Link from 'gatsby-link'

const SecondPage = () => (
  <div>
    <h1>Hi people</h1>
    <p>This page is under construction</p>
    <Link to="/">Go back to the homepage</Link>
  </div>
)

export default SecondPage
