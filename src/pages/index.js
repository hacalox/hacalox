import React, { Component } from 'react';
import './index.css';
import HeroLanding from '../components/HeroLanding';
import Menu from '../components/Menu/';
import {Bio} from './bio.js';
import Footer from '../components/Footer';
import Header from '../components/Header';
var selected = ''

export default class Landing extends Component {
  constructor(){
  super()

  this.state = {
      showBio: false,
      showProjects: false,
      showBlog: false
  }

  this.changeTab = this.changeTab.bind(this)
}

changeTab(tabName){
  var originalValue = this.state[tabName];

  this.setState({
    showBio: false,
    showProjects: false,
    showBlog: false,
    [tabName]: !originalValue
  })
}
  componentDidMount(){
    var clientHeight = document.getElementById('AppContainer').clientHeight;
    document.getElementById('AppContainer').style.height = clientHeight + 'px';
  }
  render() {
    let objeto = this.state;
     selected = Object.keys(objeto).map(function(key) {
    return objeto[key];
    }).slice(0,3).some(
     a => a === true
    ) ? true : false;

    return (
      <div className="App">
        <Header selected={selected}/>
        <div id='AppContainer' className={selected ? "AppContainerOut" : 'AppContainer'}>
          <HeroLanding selected={selected}/>
          <hr className={selected ? "LandingDividerHidden" :"LandingDivider"}/>
        </div>
        <Menu landing={true}
          selected={selected}
          landing={!selected}
          onChange={this.changeTab}
          showBio={this.state.showBio}
          showProjects={this.state.showProjects}
          showBlog={this.state.showBlog}
        />
        {!selected && <Footer/>}
        <Bio
          show={!this.state.showBio}
        />
      </div>
    )
  }
}
