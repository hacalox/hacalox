import React from 'react'
import './Bio.css'
import ProfileImage from '../assets/Hector_Anadon_Consul_3.jpg'
import GroupImage from '../assets/Hector_Anadon_Consul_4.jpg'
import {Grid, Row, Col} from 'react-flexbox-grid'

export const Bio = function(props){
  return(
    <div className={!props.show ? 'Bioshow' : 'Biohide'}>
      <Grid className='BioGrid'>
        <Row className="BioRow">
          <h1 className='BioTitle'> Who am I?</h1>
        </Row>
        <Row className="BioRow">
          <Col xs={12} sm={12} md={12} lg={6}>
              <img src={ProfileImage} alt="Hector Anadon Consul" className="BioProfileImage"/>
              <p className="BioImageFooter">
                That's me on a trekking day.<br/>
              </p>
          </Col>
          <Col xs={12} sm={12} md={12} lg={6}>
            <div className="BioFirstParagraph">
              <p>
                My name is Héctor Anadón Cónsul and I love creating new products.
                I live in Barcelona with my working partner and friend, Daniel. We are creating small businesses and trying to make a living out of it.
              </p>
              <p>
                I got into building things when I was 17 and started to build my own 3D printer.
                Since then, I have kept coding as my main method of creation, but I also have designed, calculated,
                physically built, and managed many of these products.
              </p>
              <p>
                So what is this page about? I usually start my side-projects thinking in what they will become, in how they might help other people and make me economically independent, but not in this case.
                At this very moment this web page is about keeping record of all of my projects, ideas and sharing what I've learned, but let's see what happens.
              </p>
            </div>
          </Col>
        </Row>
        <Row className="BioRow">
          <Col xs={12} sm={12} md={12} lg={6}>
            <div className="BioSecondParagraph">
              These personal facts will help you understand better who I am:
              <div className="BioSecondParagraphFacts">
                <p>
                  <strong>My two main life goals are:</strong><br/>
                  ·Creating something meaningful and profitable<br/>
                  ·Living a healthy and sustainable life (read: family and friends)<br/>
                </p>
                <p>
                  <strong>I'm concerned about:</strong><br/>
                  ·Pollution and climatic change<br/>
                  ·Privacy<br/>
                  ·Overpopulation<br/>
                </p>
                <p>
                  <strong>I mainly like projects that: </strong><br/>
                  ·I can use myself<br/>
                  ·Can get initial traction without investment<br/>
                  {/* When I see my effort become progress at the end of the day in every aspect of my life (personal, business or doing something physically very demanding). */}
                </p>
              </div>
            </div>
          </Col>
          <Col xs={12} sm={12} md={12} lg={6}>
              <img src={GroupImage} alt="Hector Anadon Consul, Daniel Carmona Serrat, and Boyan Naydenov" className="BioProfileImage"/>
                <p className="BioImageFooter">
                  There we are in our first Hackathon.<br/>
                  From left to right: Daniel, me and Boyan.
                </p>
          </Col>
        </Row>
      </Grid>
    </div>
  )
}
export default Bio;
